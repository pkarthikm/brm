CREATE TABLE object ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                      name VARCHAR(255) NOT NULL CONSTRAINT name_not_empty CHECK (name <> ''),
                      alias VARCHAR(255),
                      dbms VARCHAR(255) NOT NULL CONSTRAINT dbms_not_empty CHECK (dbms <> ''),
                      db VARCHAR(255) NOT NULL CONSTRAINT db_not_empty CHECK (db <> ''),
                      UNIQUE (name, db, dbms)
                      );
CREATE TABLE col ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                      name VARCHAR(255) NOT NULL CONSTRAINT name_not_empty CHECK (name <> ''),
                      alias VARCHAR(255),
                      is_custom BOOLEAN NOT NULL DEFAULT FALSE,
                      object_id VARCHAR(36) NOT NULL,
                      type VARCHAR(255) NOT NULL CONSTRAINT type_not_empty CHECK (type <> ''),
                      FOREIGN KEY (object_id) REFERENCES object (id),
                      UNIQUE (name, object_id)
                      );
CREATE TABLE literal ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                      value VARCHAR(255) NOT NULL CONSTRAINT value_not_empty CHECK (value <> ''),
                      type VARCHAR(255) NOT NULL CONSTRAINT type_not_empty CHECK (type <> ''),
                      dbms VARCHAR(255) NOT NULL CONSTRAINT dbms_not_empty CHECK (dbms <> ''),
                      UNIQUE (value, type, dbms)
                      );
CREATE TABLE binary_Tree ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                          object_id VARCHAR(36),
						  parent_id VARCHAR(36) CONSTRAINT parent_self_loop CHECK (parent_id <> id),
						  left_id VARCHAR(36) CONSTRAINT left_self_loop CHECK (left_id <> id),
						  right_id VARCHAR(36) CONSTRAINT right_self_loop CHECK (right_id <> id),
						  value VARCHAR(255) NOT NULL,
						  FOREIGN KEY (object_id) REFERENCES object (id),
						  FOREIGN KEY (parent_id) REFERENCES binary_Tree (id) ON DELETE CASCADE,
						  FOREIGN KEY (left_id) REFERENCES binary_Tree (id) ON DELETE CASCADE,
						  FOREIGN KEY (right_id) REFERENCES binary_Tree (id) ON DELETE CASCADE,
						  CONSTRAINT atleast_one_not_null CHECK (num_nonnulls(parent_id, left_id, right_id) > 0),
						  UNIQUE (parent_id, left_id, right_id)
						 );

CREATE UNIQUE INDEX left_uni_idx ON binary_Tree (left_id) WHERE left_id IS NOT NULL;

CREATE UNIQUE INDEX right_uni_idx ON binary_Tree (right_id) WHERE right_id IS NOT NULL;

CREATE TABLE source ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                      object_id VARCHAR(36) NOT NULL,
                      FOREIGN KEY (object_id) REFERENCES object (id)
                      );

CREATE TABLE combine ( id VARCHAR(36) NOT NULL PRIMARY KEY,
                      type VARCHAR(255) NOT NULL CONSTRAINT type_not_empty CHECK (type <> ''),
                      object_id VARCHAR(36) NOT NULL,
                      binary_tree_id VARCHAR(36) NOT NULL,
                      alias VARCHAR(255),
                      source_id VARCHAR(36) NOT NULL,
                      FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE,
                      FOREIGN KEY (object_id) REFERENCES object (id),
                      FOREIGN KEY (binary_tree_id) REFERENCES binary_tree (id) ON DELETE CASCADE
                      );


INSERT INTO binary_tree (id, parent_id, left_id, right_id, value) VALUES
    ('66dc220a-f18b-477f-a67d-23df71a8d610', null, 'd079c09b-ae14-4f01-94c7-81b608798ad9', 'cbdd4dfc-1380-41ce-8113-12e4bd51931d', '='),
    ('d079c09b-ae14-4f01-94c7-81b608798ad9', '66dc220a-f18b-477f-a67d-23df71a8d610', null, null, 'col'),
    ('cbdd4dfc-1380-41ce-8113-12e4bd51931d', '66dc220a-f18b-477f-a67d-23df71a8d610', null, null, '25');

WITH RECURSIVE children AS (
	SELECT
		id,
		parent_id,
		left_id,
		right_id,
		value
	FROM binary_Tree WHERE id = '0ab1962c-5f16-4559-a751-64d77a412697'
	UNION
		SELECT
			e.id,
			e.parent_id,
			e.left_id,
	        e.right_id,
	        e.value
		FROM
			binary_Tree e
		INNER JOIN children s ON s.id = e.parent_id
) SELECT * FROM children;
