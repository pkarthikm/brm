package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "literal")
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiteralEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "value")
    private String value;

    @Column(name = "type")
    private String type;

    @Column(name = "dbms")
    private String dbms;

    @JsonCreator
    public LiteralEntity(@JsonProperty("id") final String id,
                         @JsonProperty("value") final String name,
                         @JsonProperty("type") final String type,
                         @JsonProperty("dbms") final String dbms) throws IllegalArgumentException {
        this.value = value;
        if(StringUtils.isEmpty(this.value))
            throw new IllegalArgumentException();
        this.type = type;
        if(StringUtils.isEmpty(this.type))
            throw new IllegalArgumentException();
        this.dbms = dbms;
        if(StringUtils.isEmpty(this.dbms))
            throw new IllegalArgumentException();
        this.id = id;
        if(StringUtils.isEmpty(this.id))
            this.id = UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", value=" + value +
                    ", type=" + type +
                    ", dbms=" + dbms +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        LiteralEntity oBT = (LiteralEntity) o;
        return id.equals(oBT.id)
                && value.equals(oBT.value)
                && type.equals(oBT.type)
                && dbms.equals(oBT.dbms);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * value.hashCode()
                * type.hashCode()
                * dbms.hashCode();
    }
}
