package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "binary_tree")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BinaryTreeEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "id")
    private ObjectEntity object;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @JsonIgnore
    private BinaryTreeEntity parent;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "left_id", referencedColumnName = "id")
    private BinaryTreeEntity left;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "right_id", referencedColumnName = "id")
    private BinaryTreeEntity right;

    @Column(name = "value")
    private String value;

    @JsonCreator
    public BinaryTreeEntity(@JsonProperty("id") final String id,
                            @JsonProperty("object") final ObjectEntity object,
                            @JsonProperty("left") final BinaryTreeEntity left,
                            @JsonProperty("right") final BinaryTreeEntity right,
                            @JsonProperty("value") final String value) throws IllegalArgumentException {

        this.id = id;
        if(null == this.id)
            this.id = UUID.randomUUID().toString();
        this.object = object;
        this.left = left;
        this.right = right;
        this.value = value;
        if(null == this.value)
            throw new IllegalArgumentException();
        linkHierarchy();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        linkHierarchy();
    }

    public ObjectEntity getObject() {
        return object;
    }

    public void setObject(ObjectEntity object) {
        this.object = object;
    }

    public BinaryTreeEntity getParent() {
        return parent;
    }

    public void setParent(BinaryTreeEntity parent) {
        this.parent = parent;
    }

    public BinaryTreeEntity getLeft() {
        return left;
    }

    public void setLeft(BinaryTreeEntity left) {
        this.left = left;
        linkHierarchy();
    }

    public BinaryTreeEntity getRight() {
        return right;
    }

    public void setRight(BinaryTreeEntity right) {
        this.right = right;
        linkHierarchy();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        linkHierarchy();
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", object=" + object +
                    ", left=" + left +
                    ", right=" + right +
                    ", value=" + value +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        BinaryTreeEntity oBT = (BinaryTreeEntity) o;
        return id.equals(oBT.id)
                && parent.equals(oBT.parent)
                && object.equals(oBT.object)
                && left.equals(oBT.left)
                && right.equals(oBT.right)
                && value.equals(oBT.value);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * object.hashCode()
                * parent.hashCode()
                * left.hashCode()
                * right.hashCode()
                * value.hashCode();
    }

    /**
     * Is this a valid root node
     * @return is valid true/false
     */
    public boolean isValidRoot() {
        if(null != parent)
            return false;
        //Root but orphan - not valid
        return null != left || null != right;
    }

    /**
     * linksHierarchy and validates
     * @param isRoot Should this be validated considering to be root ?
     * @return is valid true/false
     */
    public boolean isValidTree(boolean isRoot) {
        if(isRoot && !isValidRoot())
            return false;
        if(isRoot && isValidRoot())
            linkHierarchy();
        if(!isRoot && null == left && null == right)
            return true;
        if(null != left && !equals(left.getParent()))
            return false;
        if(null != right && !equals(right.getParent()))
            return false;

        if(null == right && null != left)
            return left.isValidTree(false);
        if(null == left && null != right)
            return right.isValidTree(false);
        if(null != left && null != right)
            return left.isValidTree(false) && right.isValidTree(false);
        return true;
    }

    /**
     * Get the node and its descendents as List
     * @return List of all nodes in the tree
     */
    public List<BinaryTreeEntity> asList() {
        List<BinaryTreeEntity> nodeList = new ArrayList<>();
        addNodeToNodeList(nodeList);
        return nodeList;
    }

    /**
     * @param nodeList List to be added to
     */
    private void addNodeToNodeList(List<BinaryTreeEntity> nodeList) {
        //Nothing to add to
        if(null == nodeList)
            return;
        //Add node
        nodeList.add(this);
        //Recursively add left and right
        if(null != left)
            nodeList.addAll(left.asList());
        if(null != right)
            nodeList.addAll(right.asList());
    }

    /**
     * Recursively links child to parent starting down the given node
     * If validated before this, the validation can fail
     */
    private void linkHierarchy() {
        if(null != left) {
            left.setParent(this);
            left.linkHierarchy();
        }
        if(null != right) {
            right.setParent(this);
            right.linkHierarchy();
        }
    }
}
