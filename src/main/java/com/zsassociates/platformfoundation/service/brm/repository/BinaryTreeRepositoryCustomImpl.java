package com.zsassociates.platformfoundation.service.brm.repository;

import com.zsassociates.platformfoundation.service.brm.entity.BinaryTreeEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

public class BinaryTreeRepositoryCustomImpl implements BinaryTreeRepositoryCustom {
    private static final Logger logger = LoggerFactory.getLogger(BinaryTreeRepositoryCustomImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void saveMultipleValuesWithQuery(List<BinaryTreeEntity> binaryTreeEntities) {
        if(null == binaryTreeEntities || binaryTreeEntities.isEmpty())
            return;
        int size = binaryTreeEntities.size();
        StringBuilder sb = new StringBuilder("INSERT INTO binary_tree (id, parent_id, left_id, right_id, value) VALUES ");
        String prefix = "";
        for (BinaryTreeEntity bt : binaryTreeEntities) {
            if (null == bt)
                return;
            sb.append(prefix);
            String id = bt.getId().toString();
            if (StringUtils.isEmpty(id))
                return;
            String parent_id = null;
            BinaryTreeEntity parent = bt.getParent();
            if (null != parent)
                parent_id = parent.getId().toString();
            String left_id = null;
            BinaryTreeEntity left = bt.getLeft();
            if (null != left)
                left_id = left.getId().toString();
            String right_id = null;
            BinaryTreeEntity right = bt.getRight();
            if (null != right)
                right_id = right.getId().toString();
            String value = bt.getValue();
            sb.append("('").append(id).append("', ");
            if (StringUtils.isEmpty(parent_id))
                sb.append(parent_id).append(", ");
            else
                sb.append("'").append(parent_id).append("', ");
            if (StringUtils.isEmpty(left_id))
                sb.append(left_id).append(", ");
            else
                sb.append("'").append(left_id).append("', ");
            if (StringUtils.isEmpty(right_id))
                sb.append(right_id).append(", ");
            else
                sb.append("'").append(right_id).append("', ");
            sb.append("'").append(value).append("')");
            prefix = ",";
        }
        sb.append(";");

        logger.debug("Final Insert Query: {}", sb.toString());

        entityManager.createNativeQuery(sb.toString()).executeUpdate();
    }
}
