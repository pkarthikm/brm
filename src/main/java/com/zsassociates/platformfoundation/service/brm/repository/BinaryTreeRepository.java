package com.zsassociates.platformfoundation.service.brm.repository;

import com.zsassociates.platformfoundation.service.brm.entity.BinaryTreeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BinaryTreeRepository extends CrudRepository<BinaryTreeEntity, String>, BinaryTreeRepositoryCustom {
    @Query(value = "WITH RECURSIVE children AS ( " +
            "SELECT id, object_id, parent_id, left_id, right_id, value " +
            "FROM binary_Tree " +
            "WHERE id = :id UNION SELECT e.id, e.object_id, e.parent_id, e.left_id, e.right_id, e.value " +
            "FROM binary_Tree e " +
            "INNER JOIN children s ON s.id = e.parent_id ) SELECT * FROM children",
            nativeQuery = true)
    List<BinaryTreeEntity> findAllChildrenById(@Param("id") String id);
}
