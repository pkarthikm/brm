package com.zsassociates.platformfoundation.service.brm.repository;

import com.zsassociates.platformfoundation.service.brm.entity.BinaryTreeEntity;

import java.util.List;

public interface BinaryTreeRepositoryCustom {
    void saveMultipleValuesWithQuery(List<BinaryTreeEntity> binaryTreeEntities);
}
