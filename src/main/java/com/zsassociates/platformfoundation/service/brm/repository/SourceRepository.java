package com.zsassociates.platformfoundation.service.brm.repository;

import com.zsassociates.platformfoundation.service.brm.entity.SourceEntity;
import org.springframework.data.repository.CrudRepository;


public interface SourceRepository extends CrudRepository<SourceEntity, String>{
}
