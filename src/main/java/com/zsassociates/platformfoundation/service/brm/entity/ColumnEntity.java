package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "col")
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ColumnEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "alias")
    private String alias;

    @Column(name = "is_custom")
    private Boolean isCustom;

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "id")
    private ObjectEntity object;

    @Column(name = "type")
    private String type;

    @JsonCreator
    public ColumnEntity(@JsonProperty("id") final String id,
                        @JsonProperty("name") final String name,
                        @JsonProperty("alias") final String alias,
                        @JsonProperty("isCustom") final Boolean isCustom,
                        @JsonProperty("object") final ObjectEntity object,
                        @JsonProperty("type") final String type) throws IllegalArgumentException {
        this.name = name;
        if(StringUtils.isEmpty(this.name))
            throw new IllegalArgumentException();
        this.type = type;
        if(StringUtils.isEmpty(this.type))
            throw new IllegalArgumentException();
        this.object = object;
        if(null == this.object)
            throw new IllegalArgumentException();
        this.id = id;
        if(StringUtils.isEmpty(this.id))
            this.id = UUID.randomUUID().toString();
        this.isCustom = isCustom;
        if(null == isCustom)
            this.isCustom = false;
        this.alias = alias;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", name=" + name +
                    ", alias=" + alias +
                    ", isCustom=" + isCustom +
                    ", object=" + object +
                    ", type=" + type +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        ColumnEntity oBT = (ColumnEntity) o;
        return id.equals(oBT.id)
                && name.equals(oBT.name)
                && alias.equals(oBT.alias)
                && isCustom.equals(isCustom)
                && object.equals(object)
                && type.equals(oBT.type);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * name.hashCode()
                * alias.hashCode()
                * isCustom.hashCode()
                * object.hashCode()
                * type.hashCode();
    }
}
