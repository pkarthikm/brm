package com.zsassociates.platformfoundation.service.brm.repository;

import com.zsassociates.platformfoundation.service.brm.entity.JoinEntity;
import org.springframework.data.repository.CrudRepository;


public interface JoinRepository extends CrudRepository<JoinEntity, String>{
}
