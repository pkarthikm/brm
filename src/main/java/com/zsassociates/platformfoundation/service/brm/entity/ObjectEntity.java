package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "object")
@Data
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "alias")
    private String alias;

    @Column(name = "dbms")
    private String dbms;

    @Column(name = "db")
    private String db;

    @JsonCreator
    public ObjectEntity(@JsonProperty("id") final String id,
                        @JsonProperty("name") final String name,
                        @JsonProperty("alias") final String alias,
                        @JsonProperty("dbms") final String dbms,
                        @JsonProperty("db") final String db) throws IllegalArgumentException {
        this.name = name;
        if(StringUtils.isEmpty(this.name))
            throw new IllegalArgumentException();
        this.id = id;
        if(StringUtils.isEmpty(this.id))
            this.id = UUID.randomUUID().toString();
        this.dbms = dbms;
        if(StringUtils.isEmpty(this.dbms))
            throw new IllegalArgumentException();
        this.db = db;
        if(StringUtils.isEmpty(this.db))
            throw new IllegalArgumentException();
        this.alias = alias;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", name=" + name +
                    ", alias=" + alias +
                    ", dbms=" + dbms +
                    ", db=" + db +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        ObjectEntity oBT = (ObjectEntity) o;
        return id.equals(oBT.id)
                && name.equals(oBT.name)
                && alias.equals(oBT.alias)
                && dbms.equals(oBT.dbms)
                && db.equals(oBT.db);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * name.hashCode()
                * alias.hashCode()
                * dbms.hashCode()
                * db.hashCode();
    }
}
