package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "combine")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JoinEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "id", nullable = false)
    private ObjectEntity object;

    @OneToOne
    @JoinColumn(name = "binary_tree_id", referencedColumnName = "id", nullable = false)
    private BinaryTreeEntity on;

    @Column(name = "alias")
    private String alias;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "source_id", nullable = false)
    @JsonIgnore
    private SourceEntity source;

    @JsonCreator
    public JoinEntity(@JsonProperty("id") final String id,
                      @JsonProperty("type") final String type,
                      @JsonProperty("object") final ObjectEntity object,
                      @JsonProperty("on") final BinaryTreeEntity on,
                      @JsonProperty("alias") final String alias) throws IllegalArgumentException {
        this.type = type;
        if(StringUtils.isEmpty(this.type))
            throw new IllegalArgumentException();
        this.alias = alias;
        if(StringUtils.isEmpty(this.alias))
            throw new IllegalArgumentException();
        this.object = object;
        if(null == object)
            throw new IllegalArgumentException();
        this.id = id;
        if(StringUtils.isEmpty(this.id))
            this.id = UUID.randomUUID().toString();
        this.on = on;
        if(null == on)
            throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", type=" + type +
                    ", object=" + object +
                    ", on=" + on +
                    ", alias=" + alias +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        JoinEntity oBT = (JoinEntity) o;
        return id.equals(oBT.id)
                && type.equals(oBT.type)
                && object.equals(oBT.object)
                && on.equals(oBT.on)
                && alias.equals(oBT.alias)
                && source.equals(oBT.source);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * type.hashCode()
                * object.hashCode()
                * on.hashCode()
                * alias.hashCode()
                * source.hashCode();
    }
}
