package com.zsassociates.platformfoundation.service.brm.service;

import com.zsassociates.platformfoundation.service.brm.entity.BinaryTreeEntity;
import com.zsassociates.platformfoundation.service.brm.repository.BinaryTreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BinaryTreeService {
    @Autowired
    BinaryTreeRepository btRepository;

    /**
     *
     * @param binaryTreeEntities List of all nodes in a tree
     */
    public void saveMultipleValuesWithQuery(List<BinaryTreeEntity> binaryTreeEntities) {
        btRepository.saveMultipleValuesWithQuery(binaryTreeEntities);
    }

    /**
     *
     * @param id identity of the node to get all the children
     * @return List of all nodes in the tree
     */
    public List<BinaryTreeEntity> findAllChildrenById(String id) {
        return btRepository.findAllChildrenById(id);
    }

    /**
     * Saves Complete Tree given Root of the Tree
     * @param root This has to be root of the tree
     */
    public void saveTree(BinaryTreeEntity root) throws IllegalArgumentException {
        if(null == root)
            throw new IllegalArgumentException();
        //Validate if root
        if(!root.isValidRoot())
            throw new IllegalArgumentException();

        //Finally validate the tree
        if(!root.isValidTree(true))
            throw new IllegalArgumentException();

        List<BinaryTreeEntity> nodes = root.asList();
        if(null == nodes || nodes.isEmpty() )
            return;
        saveMultipleValuesWithQuery(nodes);
    }
}
