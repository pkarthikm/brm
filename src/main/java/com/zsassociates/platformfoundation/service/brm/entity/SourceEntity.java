package com.zsassociates.platformfoundation.service.brm.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "source")
@Data
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SourceEntity {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Id
    @Column(name = "id")
    private String id;

    @OneToMany(mappedBy = "source", fetch = FetchType.LAZY, orphanRemoval = true,
            cascade = CascadeType.ALL)
    private List<JoinEntity> joins;

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "id", nullable = false)
    private ObjectEntity object;

    @JsonCreator
    public SourceEntity(@JsonProperty("id") final String id,
                        @JsonProperty("joins") final List<JoinEntity> joins,
                        @JsonProperty("object") final ObjectEntity object
                        ) throws IllegalArgumentException {
        this.object = object;
        if(null == this.object)
            throw new IllegalArgumentException();
        this.id = id;
        if(StringUtils.isEmpty(this.id))
            this.id = UUID.randomUUID().toString();
        this.joins = joins;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException jpe){
            jpe.printStackTrace();
            return "BinaryTree{" +
                    "id='" + id + '\'' +
                    ", joins=" + joins +
                    ", object=" + object +
                    '}';
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        SourceEntity oBT = (SourceEntity) o;
        return id.equals(oBT.id)
                && joins.equals(oBT.joins)
                && object.equals(oBT.object);
    }

    @Override
    public int hashCode() {
        return (int) id.hashCode()
                * joins.hashCode()
                * object.hashCode();
    }
}
