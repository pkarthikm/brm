package com.zsassociates.platformfoundation.service.brm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrmApplication.class, args);
	}

}
