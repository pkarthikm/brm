package com.zsassociates.platformfoundation.service.brm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.zsassociates.platformfoundation.service.brm.entity.BinaryTreeEntity;
import com.zsassociates.platformfoundation.service.brm.service.BinaryTreeService;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BrmApplicationTests {

	private static final Logger logger = LoggerFactory.getLogger(BrmApplicationTests.class);

	ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@Autowired
	BinaryTreeService binaryTreeService;

	@Test
	@Order(1)
	void contextLoads() {
	}

	@Test
	@Order(2)
	@Commit
	void testBinaryTreeSave() {
		final String root_id = UUID.randomUUID().toString();

		BinaryTreeEntity field = BinaryTreeEntity.builder()
				.id(UUID.randomUUID().toString())
				.value("col")
				.parent(null)
				.left(null)
				.right(null)
				.build();
		BinaryTreeEntity fieldValue = BinaryTreeEntity.builder()
				.id(UUID.randomUUID().toString())
				.parent(null)
				.value("25")
				.left(null)
				.right(null)
				.build();
		BinaryTreeEntity equals = BinaryTreeEntity.builder()
				.id(root_id)
				.parent(null)
				.left(field)
				.right(fieldValue)
				.value("=")
				.build();

		assertTrue(equals.isValidRoot());
		assertTrue(equals.isValidTree(true));

		List<BinaryTreeEntity> binaryTreeEntityList = equals.asList();
		assertNotNull(binaryTreeEntityList);
		assertEquals(binaryTreeEntityList.size(), 3);
		binaryTreeService.saveMultipleValuesWithQuery(binaryTreeEntityList);

		List<BinaryTreeEntity> btList = fetchTreeList(root_id);
		assertNotNull(btList);
		assertEquals(btList.size(),3);
	}

	@Test
	@Order(3)
	@Commit
	void exceptionOnOrphanNode() {
		BinaryTreeEntity orphan = BinaryTreeEntity.builder()
				.id(UUID.randomUUID().toString())
				.parent(null)
				.left(null)
				.right(null)
				.value("Orphan")
				.build();

		Exception thrown = assertThrows(
				Exception.class,
				() -> binaryTreeService.saveTree(orphan),
				"Expected save() to throw Exception, but it didn't"
		);
	}

	@Test
	@Order(4)
	@Commit
	void testBinaryTreeSaveFromJson() throws JsonProcessingException {
		/*
             2
            / \
           1   10
              /
             5
        */
		final String jsonStr = "{\n"
				+ "  \"value\": \"2\",\n"
				+ "  \"left\": {\n"
				+ "    \"value\": \"1\",\n"
				+ "    \"left\": null,\n"
				+ "    \"right\": null\n"
				+ "  },\n" + "  \"right\": {\n"
				+ "    \"value\": \"10\",\n"
				+ "    \"left\": {\n"
				+ "      \"value\": \"5\",\n"
				+ "      \"left\": null,\n"
				+ "      \"right\": null\n"
				+ "    },\n"
				+ "    \"right\": null\n"
				+ "  }\n"
				+ "}";
		logger.info(jsonStr);
		final BinaryTreeEntity binaryTreeEntity = objectMapper.readValue(jsonStr, BinaryTreeEntity.class);
		assertNotNull(binaryTreeEntity);
		binaryTreeService.saveTree(binaryTreeEntity);
		String prettyJsonStr = objectMapper.writeValueAsString(binaryTreeEntity);
		assertFalse(StringUtils.isEmpty(prettyJsonStr));
		logger.info("Binary Tree Json: \n{}", prettyJsonStr);
		//Fetch and assert
		assertFalse(StringUtils.isEmpty(binaryTreeEntity.getId()));
		List<BinaryTreeEntity> btList = fetchTreeList(binaryTreeEntity.getId());
		assertNotNull(btList);
		assertEquals(btList.size(),4);
		//final List<Integer> listExpected = Arrays.asList(2, 1, 10, 5);
		//assertEquals(listExpected, intTree.preOrder());
	}


	private List<BinaryTreeEntity> fetchTreeList(String id) throws IllegalArgumentException {
		if(StringUtils.isEmpty(id))
			throw new IllegalArgumentException();
		List<BinaryTreeEntity> binaryTreeEntityList = binaryTreeService.findAllChildrenById(id);
		for(BinaryTreeEntity bt: binaryTreeEntityList) {
			assertNotNull(bt);
			assertFalse(StringUtils.isEmpty(bt.getId()));
		}
		assertNotNull(binaryTreeEntityList);
		assertFalse(binaryTreeEntityList.isEmpty());
		return binaryTreeEntityList;
	}

}
